import { Component, OnInit } from '@angular/core';
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})




export class AppComponent implements OnInit {


  
  title = 'af-notification';
  message:any = null;
  constructor() {}
  ngOnInit(): void {




    this.requestPermission();
    this.listen();
  }
  requestPermission() {
    const firebaseConfig = {
      apiKey: "AIzaSyCexpPcb3bOGqE8QAu2vLGx3cQipTohaf4",
      authDomain: "blinding-heat-8445.firebaseapp.com",
      databaseURL: "https://blinding-heat-8445.firebaseio.com",
      projectId: "blinding-heat-8445",
      storageBucket: "blinding-heat-8445.appspot.com",
      messagingSenderId: "875697362711",
      appId: "1:875697362711:web:4bda94c9bd8f75b93b8952",
      measurementId: "G-VM6549SZCH"
    };
    
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    
    const messaging = getMessaging(app);
    //const messaging = getMessaging();
    getToken(messaging, 
     { vapidKey: 'BLJ6arEKUOJU_Ne6LYDVOn06hJoFqIEQqBAEMiSrgBLGXInjwk8f3VwhWkkvMejOsV9jVXiB9pqLCYNvbXko1qA'}).then(
       (currentToken) => {
         if (currentToken) {
           console.log("Hurraaa!!! we got the token.....");
           console.log(currentToken);
         } else {
           console.log('No registration token available. Request permission to generate one.');
         }
     }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
    });
  }
  listen() {
    const messaging = getMessaging();
    onMessage(messaging, (payload) => {
      console.log('Message received. ', payload);
      this.message=payload;
    });
  }


  
 

  tester(){
  

    console.log("TESTER run");
  }


}


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional







