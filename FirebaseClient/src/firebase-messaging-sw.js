// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
    apiKey: "AIzaSyCexpPcb3bOGqE8QAu2vLGx3cQipTohaf4",
    authDomain: "blinding-heat-8445.firebaseapp.com",
    databaseURL: "https://blinding-heat-8445.firebaseio.com",
    projectId: "blinding-heat-8445",
    storageBucket: "blinding-heat-8445.appspot.com",
    messagingSenderId: "875697362711",
    appId: "1:875697362711:web:4bda94c9bd8f75b93b8952",
    measurementId: "G-VM6549SZCH"
  };
firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

